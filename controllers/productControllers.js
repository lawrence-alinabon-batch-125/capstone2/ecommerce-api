const Product = require('./../models/Product')

module.exports.addProduct = (reqBody) => {
    let newProduct = new Product ({
        productName: reqBody.productName,
        productDesc: reqBody.productDesc,
        productPrice: reqBody.productPrice
    })
    return newProduct.save().then((result, error) => {
        if (error) {
            return error;
        } else {
            return result;
        }
    })
}

module.exports.showProducts = () => {
    return Product.find().then(result => {
        return result
    } );
}

module.exports.showActiveProducts = () => {
    return Product.find({"isActive" : true}).then(result => {
        return result
    } );
}

module.exports.showSingleProduct = (paramsId) => {
    return Product.findById(paramsId).then(product => product);
}

module.exports.editProduct = (paramsId, reqBody) => {
    let updatedProduct = {
        productName : reqBody.productName,
        productDesc : reqBody.productDesc,
        productPrice : reqBody.productPrice 
    }

    return Product.findByIdAndUpdate(paramsId, updatedProduct, { new: true }).then((result, error) => {
        if (error) {
            return error
        } else {
            return result;
        }
    })
}

module.exports.archiveProduct = (paramsId) => {
    let setUnactive = {
        isActive : false
    }

    return Product.findByIdAndUpdate(paramsId, setUnactive, { new: true }).then((result, error) => {
        if (error) {
            return false
        } else {
            return true
        }
    })
}

module.exports.unarchiveProduct = (paramsId) => {
    let setActive = {
        isActive : true
    }

    return Product.findByIdAndUpdate(paramsId, setActive, { new: true }).then((result, error) => {
        if (error) {
            return false
        } else {
            return true
        }
    })
}
