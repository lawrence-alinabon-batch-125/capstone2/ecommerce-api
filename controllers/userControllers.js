const User = require('./../models/User');
const bcrypt = require('bcrypt');
const auth = require('./../auth');

// register user
module.exports.register = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((result, error) => {
		if (error) {
			return error;
		} else {
			return true;
		}
	});
};
module.exports.checkEmailExist = (reqBody) => {
	return User.find({ email: reqBody.email }).then((result, error) => {
		if (result.length != 0) {
			return true;
		} else {
			return false;
		}
	});
};

module.exports.login = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			);
			if (isPasswordCorrect === true) {
				return { access: auth.createAccessToken(result.toObject()) };
			} else {
				return false;
			}
		}
	});
};

module.exports.setAdmin = (paramId) => {
	let updateUser = {
		isAdmin: true,
	};
	
	return User.findByIdAndUpdate(paramId, updateUser, { new: true }).then(
		(result, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}
	);
};

module.exports.unsetAdmin = (paramId) => {
	let updateUser = {
		isAdmin: false,
	};

	return User.findByIdAndUpdate(paramId, updateUser, { new: true }).then(
		(result, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}
	);
};

module.exports.getProfile = (data) => {
	return User.findById(data, {"cart._id" : 0}).then((result) => {
		result.password = '*******';
		return result;
	});
};

module.exports.addToCart = (data, reqBody) => {

	let pdata = {
		productId: reqBody.productId,
		productPrice: reqBody.productPrice,
		productName: reqBody.productName
	};
	return User.findById(data.userId).then(user => {
		
        user.cart.push(pdata)

        return user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })
    })
}