const Product = require('../models/Product');
const Order = require('./../models/Order')
const User = require('./../models/User');


module.exports.createOrder = async(userId, reqBody) => {
    let newOrder = new Order({
        totalAmount: reqBody.totalAmount,
        userId: userId,
        productsOrdered: reqBody.productsOrdered
    })

    const saveOrder = await newOrder.save().then((result, error) => {
        if (error) {
            return error;
        } else {
            return result;
        }

    })

    const deletecart = await User.updateOne({_id: userId }, {cart : []}).then((user, error) => {
		if (error) {
            return false;
        } else {
            return true;
        }
	})

	if (saveOrder && deletecart) {
        return true
    } else {
        return false
    }


}

module.exports.viewOrders = () => {
    return Order.find().then(result => result);
}

module.exports.userOrders = (userId) => {

    return Order.find({ userId: userId }).then((result, error) => {
        if (error) {
            return error
        } else {
            return result
        }
    });
}
