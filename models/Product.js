const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, "Product name is required"]
    },
    productDesc: {
        type: String,
        required: [true, "Product description is required"]
    },
    productPrice:{
        type : Number,
        required:[ true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
})

module.exports = mongoose.model("Products", productSchema);