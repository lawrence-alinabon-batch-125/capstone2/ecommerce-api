const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    mobileNo:{
        type : String,
        required:[ true, "Mobile number is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type : String,
        required:[ true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    totalAmount: {
        type: Number
    },
    cart: [
        {
            productId: {
                type: String,
                required: [true, "Product is required"]

            },
            productPrice: {
                type: Number,
                required: [true, "Product is required"]

            },
            productName: {
                type: String,
                required: [true, "Product is required"]

            }
        }
    ]
})

module.exports = mongoose.model("Users", userSchema);