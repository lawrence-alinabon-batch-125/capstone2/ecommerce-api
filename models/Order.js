const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, "First name is required"]
    },
    userId: {
        type: String,
        required: [true, "Product is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    productsOrdered: [
        {
            productId: {
                type: String,
                required: [true, "Product is required"]

            },
            productPrice: {
                type: Number,
                required: [true, "Product is required"]

            },
            productName: {
                type: String,
                required: [true, "Product is required"]

            }
        }
    ]
})

module.exports = mongoose.model("Orders", orderSchema);