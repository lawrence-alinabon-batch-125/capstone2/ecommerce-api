const express = require( 'express' );
const mongoose = require( 'mongoose' );
const PORT = process.env.PORT || 3001;
const app = express();
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")
const cors = require('cors');



app.use( express.json() );
app.use( express.urlencoded( {extended:true} ) );
app.use( cors() );

mongoose.connect("mongodb+srv://lawrence:admin@cluster0.psfw2.mongodb.net/ecommerce?retryWrites=true&w=majority",
    {
        useNewUrlParser:true,
	    useUnifiedTopology:true
    }
).then( () => { 
	console.log( `Successfully Connected to Database!` );
} ).catch( ( error ) => { 
	console.log( error );
} );



app.use("/ecommerce/api/users", userRoutes);
app.use("/ecommerce/api/products", productRoutes);
app.use("/ecommerce/api/orders", orderRoutes);



app.listen(PORT, () => {
    console.log( `Server started on port`, PORT );
} );