const express = require('express');
const router = express.Router();
let auth = require('./../auth');
const orderController = require('./../controllers/orderControllers');

router.post('/checkout', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if (userData.isAdmin === false) {
        orderController.createOrder(userData.id, req.body).then(result => res.send(result));
    } else {
        res.send(false);
    }
    
});

router.get('/', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin === true) {
            orderController.viewOrders().then(result => res.send(result))
    } else {
        res.send(false);
    }
      
});

router.get('/myOrders', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin === false) {
        orderController.userOrders(userData.id).then(result => res.send(result))
    } else {
        res.send(false);
    }
      
});

module.exports = router;