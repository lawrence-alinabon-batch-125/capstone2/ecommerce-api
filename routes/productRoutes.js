const express = require('express');
const router = express.Router();
let auth = require('./../auth');
const productController = require('./../controllers/productControllers');

router.post('/addProduct', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin === true) {
        productController.addProduct(req.body).then(result => res.send(result));
    } else {
        res.send(false);
    }
    
});

router.get('/all', auth.verify, (req, res) => {
    
    productController.showProducts().then(result => res.send(result));
});

router.get('/:productId/singleProduct', auth.verify, (req, res) => {
    productController.showSingleProduct(req.params.productId).then(result => res.send(result));
});

router.put('/:productId/updateProduct', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if (userData.isAdmin === true) {
        productController.editProduct(req.params.productId, req.body).then(result => res.send(result));
    } else {
        res.send(false);
    }
});

router.put('/:productId/archiveProduct', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin === true) {
        productController.archiveProduct(req.params.productId).then(result => res.send(result));
    } else {
        res.send(false);
    }
});

router.put('/:productId/unarchiveProduct', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin === true) {
        productController.unarchiveProduct(req.params.productId).then(result => res.send(result));
    } else {
        res.send(false);
    }
});

router.get('/active', auth.verify, (req, res) => {
    
    productController.showActiveProducts().then(result => res.send(result));
});

module.exports = router;