const express = require('express');
const router = express.Router();
const auth = require('./../auth');
const userController = require('./../controllers/userControllers');

router.post('/register', (req, res) => {
	userController.register(req.body).then( result => res.send(result) );
});

router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then( result => res.send( result ) );
});

router.post('/login', (req, res) => {
	userController.login(req.body).then( result => res.send( result ) );
});

router.put('/:userId/setAsAdmin', auth.verify, ( req, res) => {
	userController.setAdmin(req.params.userId).then( result => res.send( result ) );
});

router.put('/:userId/unsetAsAdmin', auth.verify, (req, res) => {
	userController.unsetAdmin(req.params.userId).then( result => res.send(result) );
});

router.get('/profile', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// console.log(userData);
	userController.getProfile(userData.id).then( result => res.send(result) );
});

router.post('/addtocart', (req, res) => {
	let data = {
        userId: auth.decode(req.headers.authorization).id
	}
	userController.addToCart(data, req.body).then((result) => res.send(result));

});
module.exports = router;
